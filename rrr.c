/*
 ============================================================================
 Name        : rrr.c
 Author      : 
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>


int main(void)
{
	int x, y;

	printf("Enter 2 integer numbers: ");
	fflush(stdout);
	scanf("%d%d", &x, &y);
	printf("First Number: %d\n", x);
	printf("Second Number: %d\n", y);
	printf("Addition Result: %d\n", x+y);
	printf("Subtraction Result: %d\n", x-y);
	printf("Multiplication Result: %d\n", x*y);
	printf("Division Result: %d\n", x/y);
	printf("Remainder Result %d\n", x%y);

	return 0;
}
